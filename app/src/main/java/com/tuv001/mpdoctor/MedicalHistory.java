package com.tuv001.mpdoctor;
/**
 * Created by JTJ
 */

//GUI (JTJ)

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
//Web service (Magommed)
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;






public class MedicalHistory extends AppCompatActivity {

    private ArrayList<History> historyList;
    private ListView lv;
    private Button buttonMHsend;//Send medical history request
    private EditText firstname;
    private EditText lastname;
    private String enteredfirstname;//Patient's firstname
    private String enteredlastname;//Patient's lastname

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medical_history);

        lv=(ListView) findViewById(R.id.listView);
        buttonMHsend=(Button)findViewById(R.id.buttonSendMedicalHistory);
        firstname=(EditText)findViewById(R.id.etpatientfirstname);
        lastname=(EditText)findViewById(R.id.etpatientlastname);

        //ButtonSEndMedicalHistory
        buttonMHsend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                enteredfirstname = firstname.getText().toString();
                enteredlastname = lastname.getText().toString();

                if (enteredlastname.equals("") || enteredfirstname.equals("")) {
                    Toast.makeText(MedicalHistory.this, getString(R.string.messagefillhistory), Toast.LENGTH_LONG).show();
                    //Error msg if last name or first name is equals " "
                    return;
                }


                //Created by Magommed from *
                else {
                        getMedicalHistory();
                }// to *


            }
        });


    }

    //Method getMedicalHistory created by JTJ
    private void getMedicalHistory()
    {
        gethistoryData(enteredfirstname,enteredlastname);// Receive the data from the DB
        //ADAPTER
        final HistoryAdapter adapter=new HistoryAdapter(this, historyList); //
        lv.setAdapter(adapter);
    }


    //Method gettranslation created by JTJ
    //Method that translates disease into Spanish or English
    private String gettranslation(String disease)
    {
        Map<String,String> english=new HashMap<String, String>();
        Map<String,String> spanish=new HashMap<String, String>();
        english.put("Lebensmittelvergiftung","Food poisoning"); spanish.put("Lebensmittelvergiftung","Intoxicación Alimentaria");
        english.put("Grippe","Flu");spanish.put("Grippe","Gripe");
        english.put("Erkältung","Cold");spanish.put("Erkältung","Resfriado");
        english.put("Migräne","Migraine");spanish.put("Migräne","Migraña");
        english.put("Gicht","Gout");spanish.put("Gicht","Gota");
        english.put("Blinddarmentzündung","Appendicitis");spanish.put("Blinddarmentzündung","Apendicitis");
        english.put("Mandelentzündung","Amygdalitis");spanish.put("Mandelentzündung","Amigdalitis");
        english.put("Heuschnupfen","Hay fever");spanish.put("Heuschnupfen","Fiebre del heno");
        english.put("Lungenentzündung","Pneumonia");spanish.put("Lungenentzündung","Neumonía");
        english.put("Mittelohrentzündung","Otitis");spanish.put("Mittelohrentzündung","Otitis");

        //Get the current language
        String language= Locale.getDefault().getLanguage();

        if(language.equals("es"))
        {
            disease=spanish.get(disease);
        }
        else if(language.equals("en"))
        {
            disease=english.get(disease);
        }
        return disease;
    }


    // Method gethistoryData from Magomed
    private void gethistoryData(String firstname,String lastname) {

        //Adding data to ArrayList
        historyList = new ArrayList<History>();
        RequestParams params = new RequestParams();
        params.put("Lastname", enteredlastname);
        params.put("Firstname", enteredfirstname);
        // Invoke RESTful Web Service with Http parameters
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://192.168.178.60:8080/MedicineWebservice/doctor/patientHistory", params, new AsyncHttpResponseHandler() {


            @Override
            public void onSuccess(String response) {
                try {
                    // JSON Object
                    JSONObject obj = new JSONObject(response);
                    JSONArray firstJsonArr = obj.getJSONArray("array1");
                    JSONArray secondJsonArr = obj.getJSONArray("array2");
                    JSONArray thirdJsonArr = obj.getJSONArray("array3");
                    // anzahl History patienten not implemented yet, thats why for test 2
                    for (int i = 0; i < 2; i++) {
                        historyList.add(new History((String) firstJsonArr.get(i), gettranslation((String) secondJsonArr.get(i)), (String) thirdJsonArr.get(i)));

                    }


                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(MedicalHistory.this, getResources().getString(R.string.errorconnection), Toast.LENGTH_LONG).show(); //JTJ GUI
                    e.printStackTrace();

                }


            }
        });
    }


}
