package com.tuv001.mpdoctor;

/**
 * Created by JTJ-PC on 09.06.2017.
 */

public class History
{
    String date;
    String disease;
    String medicine;

    public History(String date, String disease, String medicine)
    {
        this.date = date;
        this.disease = disease;
        this.medicine = medicine;
    }

    public String getDate()
    {
        return date;
    }

    public void setDate(String date)
    {
        this.date = date;
    }

    public String getDisease()
    {
        return disease;
    }

    public void setDisease(String disease)
    {
        this.disease = disease;
    }

    public String getMedicine()
    {
        return medicine;
    }

    public void setMedicine(String medicine)
    {
        this.medicine = medicine;
    }
}
