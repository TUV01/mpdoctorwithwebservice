package com.tuv001.mpdoctor;
//GUI JTJ
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
//Web Service Magommed
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;



public class Patienten extends AppCompatActivity {


    private ArrayList<Patient> patientsList;
    private ListView lv;
    private SearchView sv;


    // @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patienten);

        lv=(ListView) findViewById(R.id.listView);
        sv=(SearchView) findViewById(R.id.searchView);

        getPatientData(); // Receive the data from the DB

        //ADAPTER
        final PatientAdapter adapter=new PatientAdapter(this, patientsList); //
        lv.setAdapter(adapter);

        //Search function
        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String arg0) {
                // TODO Auto-generated method stub
                return false;
            }
            @Override
            public boolean onQueryTextChange(String query) {
                // TODO Auto-generated method stub
                adapter.getFilter().filter(query);
                return false;
            }
        });
    }


    private void getPatientData() {
        //Adding data to ArrayList
        patientsList = new ArrayList<Patient>();
        RequestParams params = new RequestParams();
        // Invoke RESTful Web Service with Http parameters
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://194.95.223.191:8080/MedicineWebservice/medicament/medrating", params, new AsyncHttpResponseHandler() {


            @Override
            public void onSuccess(String response) {
                try {
                    // JSON Object
                    JSONObject obj = new JSONObject(response);
                    JSONArray firstJsonArr= obj.getJSONArray("array1");
                    JSONArray secondJsonArr= obj.getJSONArray("array2");
                    JSONArray thirdJsonArr = obj.getJSONArray("array3");
                    for (int i = 0; i < 7; i++) {
                        patientsList.add(new Patient((String) firstJsonArr.get(i),(String) secondJsonArr.get(i),gettranslation((String) thirdJsonArr.get(i))));

                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(Patienten.this, getResources().getString(R.string.errorconnection), Toast.LENGTH_LONG).show();
                    e.printStackTrace();

                }


            }
        });



    }

    //Method that translates status into Spanish or English
    private String gettranslation(String status)
    {
        Map<String,String> english=new HashMap<String, String>();
        Map<String,String> spanish=new HashMap<String, String>();
        english.put("Ja","Yes"); spanish.put("Ja","Si");
        english.put("Nein","No"); spanish.put("Nein","No");

        //Get the current language
        String language= Locale.getDefault().getLanguage();

        if(language.equals("es"))
        {
            status=spanish.get(status);
        }
        else if(language.equals("en"))
        {
            status=english.get(status);
        }
        return status;
    }


}
